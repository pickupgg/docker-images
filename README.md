# Docker Images

This is a collection of Docker images used for building dedicated game servers for pickup.gg.

## Project structure

Each directory in the project contains the source for a particular game server image. For the most part, they look like:

```
/game-abbrev
  bin/
    run-server
  docker-entrypoint.d/
    00-write-configurations
    01-download-the-game
  config1.cfg
  config2.cfg
  otherfile.txt
  Dockerfile
  README.md
```

The `Dockerfile` contains build instructions for the image. The Dockerfile always starts with one of the base images: `base`, or `steam` in the case of Steam-based games.

The `docker-entrypoint.d` directory contains scripts that will be run when the container starts up. The scripts are run in alphabetical order (hence the numeric prefix), and their names may only contain alphanumeric characters, dashes, and underscores, and they cannot have file extensions. Each init script must also being with a shebang to tell the shell what program to use to run it (e.g., `#!/usr/bin/env python3`).

The `bin/` directory contains scripts that should be called as part of the `CMD` in the `Dockerfile`. Usually they start the game server.

The `README.md` will provide details on how the image is to be built and run.

There may be other files as well, and they are generally being copied into the image as part of setup.

# Building the project

Building the images can be done with the `./build` script. It will build all of the images in the order specified in the `IMAGES` variable in the script.
