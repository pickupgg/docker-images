#!/usr/bin/env bash

PARTS_DIR=/docker-entrypoint.d

main () {
    if [[ -d "$PARTS_DIR" ]]
    then
        # This will run everything in $PARTS directory. All scripts to be run
        # should be named with only alphanumeric characters, hyphens, and
        # underscores. Thus, the scripts should not have an extension, and must be
        # marked as executable.
        /bin/run-parts --verbose "$PARTS_DIR"
    fi

    /usr/bin/env bash -c "$@"
}

main "$@"
